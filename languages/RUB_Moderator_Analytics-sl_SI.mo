��          �   %   �      @     A     Y     q     �     �  
   �     �     �     �  '   �  +   �  
   %     0  �   7  .   �  4   �     (     E     I     b  .   {  4   �     �  	   �  �  �     �     �  %   �               $     5     P     _  .   t  8   �     �     �  �   �  >   �  ?   �  #   	     >	     C	     ^	  ,   ~	  :   �	     �	     �	                                               
                                                       	                        #distinct forum readers #distinct topic readers #distinct writers #forums read #logins #news read #replies written #topics read #topics written Additional info for data interpretation Emails of moderators who should get summary Group Name Hello, Interpret number of logins with a grain of salt, since users can opt to have their session remembered by the platform thus omitting log-ins. List of user IDs not to include in the summary List of user IDs who should be labelled as moderator Moderator Analytics Settings No. Separate values by ","!! Site Analytics for CW %d This activity happened during calendar week %d This happened in the week before (calendar week: %d) Total user type Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-11 11:18+0100
PO-Revision-Date: 2016-03-11 11:29+0100
Last-Translator: 
Language-Team: 
Language: sl_SI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 1.8.6
X-Poedit-Bookmarks: -1,-1,-1,-1,-1,-1,-1,-1,-1,3
 št. različnih bralcev forumov št. različnih bralcev tem št. različnih uporabnikov z zapisom št. branj forumov št. prijav št. branj novic št. objavljenih odgovorov št. branj tem št. objavljenih tem Dodatne informacije za interpretacijo podatkov E-poštni naslovi moderatorjev, ki naj prejmejo povzetek Ime skupine Pozdravljeni! Pri interpretaciji števila prijav je potrebno upoštevati, da so lahko uporabniki izbrali možnost shranjevanja seje, da se jim ne bi bilo treba ponovno prijaviti. Seznam oznak uporabnikov, ki naj ne bodo vključeni v povzetek Seznam oznak uporabnikov, ki naj bodo označeni kot moderatorji Nastavitve analitike za moderatorja Št. Vrednosti ločite z  ","!! Analiza Učne platforme v KT %d To se je zgodilo v koledarskem tednu št. %d To se je zgodilo v prejšnjem tednu (koledarski teden: %d) Skupaj vrsta uporabnika 