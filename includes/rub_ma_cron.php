<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 03-Mar-16
 * Time: 17:07
 */

/**
 * This function registers the action hook as cron event
 */
function rub_ma_set_schedule()
{
    // schedule digest for 8 in the morning
    $sending_date = new DateTime();
    $sending_date->setTime(8, 0);

    // only allows daily, hourly, and twicedaily (https://codex.wordpress.org/Function_Reference/wp_schedule_event)
    wp_schedule_event(time(), 'daily', 'rub_ma_weekly_summary');
}

// this connects the schedule with the method to send the digest
add_action('rub_ma_weekly_summary', 'rub_ma_send_summary');

/**
 * Contains code to send out period email containing analytics data
 * @param bool|false $send_directly
 */
function rub_ma_send_summary($send_directly = false){

    // bail if today isn't monday
    $dateTime = new DateTime();

    if($send_directly == false) {
        if ($dateTime->format('N') != 1) {
            return;
        }
    }

    // fetch results for this week and last week
    $user_results_current_week = rub_ma_get_user_stats_from_db(1); // last week (before monday)
    $user_results_last_week = rub_ma_get_user_stats_from_db(2); // the week before

    // don't send stuff if there is no result
    if(count($user_results_last_week) == 0 && count($user_results_current_week) == 0){
        return;
    }

    // compute time spans of the calendar weeks

    // get calendar week first
    $current_calendar_week = date('W');
    // Create ISODate based on year and calendar week, afterwards get last day of week by simple offset
    $date_week_start_current_week = new DateTime();
    $date_week_start_current_week->setISODate($date_week_start_current_week->format('Y'), $current_calendar_week - 1);
    $date_week_end_current_week = new DateTime();
    $date_week_end_current_week->setISODate($date_week_end_current_week->format('Y'), $current_calendar_week - 1);
    $date_week_end_current_week = $date_week_end_current_week->modify('+6 day');

    $date_week_start_previous_week = new DateTime();
    $date_week_start_previous_week->setISODate($date_week_start_previous_week->format('Y'), $current_calendar_week - 2);
    $date_week_end_previous_week = new DateTime();
    $date_week_end_previous_week->setISODate($date_week_end_previous_week->format('Y'), $current_calendar_week - 2);
    $date_week_end_previous_week = $date_week_end_previous_week->modify('+6 day');


    $mail_body = __('Hello,','RUB_Moderator_Analytics');
    $mail_body .= "<br>";
    $mail_body .= "<br>";
    $mail_body .= sprintf(esc_html__('This activity happened during calendar week %d','RUB_Moderator_Analytics'), $current_calendar_week - 1); // get analysis for last week (since we mail this on Mondays)
    $mail_body .= " (" . $date_week_start_current_week->format('d.m.Y') . " - " . $date_week_end_current_week->format('d.m.Y') . "):";
    $mail_body .= "<br>";
    // add user stats this week
    $mail_body .= rub_ma_create_user_stats_html_table_for_email($user_results_current_week);
    $mail_body .= "<br>";
    // add group stats this week
    $mail_body .= rub_ma_create_group_stats_html_table_for_email(
        rub_ma_get_group_write_stats_from_db(1),
        rub_ma_get_group_forum_read_stats_from_db(1),
        rub_ma_get_group_topic_read_stats_from_db(1),
        rub_ma_get_list_of_all_groups());
    $mail_body .= __('Number of newly registered users:','RUB_Moderator_Analytics') . " " . rub_ma_get_number_of_registrations(1);
    $mail_body .= "<br>";
    $mail_body .= "<br>";
    $mail_body .= sprintf(esc_html__('This happened in the week before (calendar week: %d)','RUB_Moderator_Analytics'), $current_calendar_week - 2); // and the week before
    $mail_body .= " (" . $date_week_start_previous_week->format('d.m.Y') . " - " . $date_week_end_previous_week->format('d.m.Y') . "):";
    // add group stats this week
    $mail_body .= rub_ma_create_user_stats_html_table_for_email($user_results_last_week);
    $mail_body .= "<br>";
    // add group stats this week
    $mail_body .= rub_ma_create_group_stats_html_table_for_email(
        rub_ma_get_group_write_stats_from_db(2),
        rub_ma_get_group_forum_read_stats_from_db(2),
        rub_ma_get_group_topic_read_stats_from_db(2),
        rub_ma_get_list_of_all_groups());
    $mail_body .= __('Number of newly registered users:','RUB_Moderator_Analytics') . " " . rub_ma_get_number_of_registrations(2);
    $mail_body .= "<br>";
    $mail_body .= "<br>";
    $mail_body .= __('Additional info for data interpretation:','RUB_Moderator_Analytics');
    $mail_body .= "<br>";
    $mail_body .= __('Interpret number of logins with a grain of salt, since users can opt to have their session remembered by the platform thus omitting log-ins.','RUB_Moderator_Analytics');

    // get all moderator's email addresses
    $array_mails = get_option('RUB_ma_emails_id');
    $array_mails = explode(',',$array_mails);

    $site_title = get_bloginfo('name');
    $site_title = __($site_title, 'RUB_Moderator_Analytics');
    $mailsubject = $site_title . " - " . sprintf(esc_html__('Site Analytics for CW %d','RUB_Moderator_Analytics'), date('W')-1);

    // Check if summary is to be send out immediately (e.g. ordered by user through backend)
    if($send_directly){
        $mail = get_option('RUB_ma_emails_id_extra'); // Fetch new email
        $send = wp_mail($mail, $mailsubject, $mail_body, "Content-type: text/html"); // send stuff

        if(!$send){
            rub_logger_add("Mail send failed internally, to " . $mail);
        }
    } else {
        // send mails for normal
        foreach($array_mails as $mail){
            $send = wp_mail($mail,$mailsubject, $mail_body, "Content-type: text/html");

            if(!$send){
                rub_logger_add("Mail send failed internally, to " . $mail);
            }
        }
    }
}