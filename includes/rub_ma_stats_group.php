<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 09-Mar-16
 * Time: 13:49
 */

/**
 * Which group got how many written posts by how many persons
 * @param int $weekoffset
 * @return mixed
 */
function rub_ma_get_group_write_stats_from_db($weekoffset = 0){
    global $wpdb;
    $groups_table = $wpdb->prefix . 'bp_groups';
    $groupmeta_table = $wpdb->prefix . 'bp_groups_groupmeta';

    $results = $wpdb->get_results(
        "SELECT
            pmeta.meta_value AS 'forum_id',
            (SELECT post_title FROM $wpdb->posts WHERE ID = pmeta.meta_value) AS 'forum_label',
            groups.status AS 'visibility',
            SUM(IF(post_type = 'topic', 1, 0)) AS 'topics_written',
            SUM(IF(post_type = 'reply', 1, 0)) AS 'replies_written',
            COUNT(DISTINCT(post_author)) AS 'active_writers'
        FROM $wpdb->posts posts
        JOIN $wpdb->postmeta pmeta ON pmeta.post_id = posts.ID
        JOIN $groups_table groups ON groups.slug = (SELECT post_name FROM $wpdb->posts WHERE ID = pmeta.meta_value)
        WHERE
            post_status = 'publish'
            AND pmeta.meta_key = '_bbp_forum_id'
            AND WEEKOFYEAR(post_date_gmt) = WEEKOFYEAR(NOW()) - $weekoffset
            AND YEAR(post_date_gmt) = YEAR(NOW())
            AND posts.post_author NOT IN (" . get_option('RUB_ma_user_blacklist_id') . ")
        GROUP BY
            pmeta.meta_value"
    );

    return $results;
}

/**
 * Which forum got how many reads by how many persons
 * @param int $weekoffset
 * @return mixed
 */
function rub_ma_get_group_forum_read_stats_from_db($weekoffset = 0){

    global $wpdb;
    $log_table = $wpdb->prefix . 'aryo_activity_log';

    $results = $wpdb->get_results(
        "SELECT
            log.object_id AS 'forum_id',
            (SELECT post_title FROM $wpdb->posts WHERE ID = log.object_id) AS 'forum_label',
            SUM(IF(log.action = 'read' AND log.object_subtype = 'page', 1, 0)) AS 'forums_read',
            COUNT(DISTINCT(user_id)) AS 'active_readers_forum'
        FROM $log_table AS log
        WHERE
            log.object_id IN (SELECT ID FROM $wpdb->posts WHERE post_type = 'forum')
            AND WEEKOFYEAR(FROM_UNIXTIME(hist_time)) = WEEKOFYEAR(NOW()) - $weekoffset
            AND YEAR(FROM_UNIXTIME(hist_time)) = YEAR(NOW())
            AND log.user_id NOT IN (" . get_option('RUB_ma_user_blacklist_id') . ")
        GROUP BY
            log.object_id"
    );

    return $results;
}

/**
 * Which topic got how many reads by how many persons
 * @param int $weekoffset
 * @return mixed
 */
function rub_ma_get_group_topic_read_stats_from_db($weekoffset = 0){

    global $wpdb;
    $log_table = $wpdb->prefix . 'aryo_activity_log';

    $results = $wpdb->get_results(
        "SELECT
            (SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = '_bbp_forum_id' AND post_id = log.object_id) AS 'forum_id',
            SUM(IF(log.action = 'read' AND log.object_subtype = 'Topics', 1, 0)) AS 'topics_read',
            COUNT(DISTINCT(user_id)) as 'active_readers_topics'
        FROM $log_table AS log
        WHERE
            log.object_id IN (SELECT ID FROM $wpdb->posts WHERE post_type = 'topic')
            AND WEEKOFYEAR(FROM_UNIXTIME(hist_time)) = WEEKOFYEAR(NOW()) - $weekoffset
            AND YEAR(FROM_UNIXTIME(hist_time)) = YEAR(NOW())
            AND log.user_id NOT IN (" . get_option('RUB_ma_user_blacklist_id') . ")
        GROUP BY
            forum_id"
    );

    return $results;
}

/**
 * Gets a list of all groups. Used to avoid empty group names resulting from the inferior query above
 * @return mixed
 */
function rub_ma_get_list_of_all_groups(){
    global $wpdb;
    $groups_table = $wpdb->prefix . 'bp_groups';
    $groupsmeta_table = $wpdb->prefix . 'bp_groups_groupmeta';

    $results = $wpdb->get_results(
        "SELECT p.ID as 'forum_id', g.name as 'forum_label', g.status as 'visibility', gmeta.meta_value AS 'total_members'
        FROM $groups_table g
        JOIN $wpdb->posts p ON p.post_name = g.slug
        JOIN $groupsmeta_table gmeta ON g.id = gmeta.group_id
        WHERE p.post_type = 'forum'
        AND gmeta.meta_key = 'total_member_count'"
    );

    return $results;
}

/**
 * Assemble table with info from db
 * @param $write_results
 * @param $read_forum_results
 * @param $read_topic_results
 * @param $all_groups
 * @return string
 */
function rub_ma_create_group_stats_html_table_for_email($write_results, $read_forum_results, $read_topic_results, $all_groups){

    $mail_body  = '<table style="border: 1px solid">';
    $mail_body .= '<tr>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('No.','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('Group Name','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('Visibility','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#topics written','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#replies written','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#distinct writers / all','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#forums read','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#topics read','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#distinct forum readers','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-bottom: 1px solid">' . __('#distinct topic readers','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= "</tr>";

    $results_array = rub_ma_merge_group_arrays($write_results, $read_forum_results, $read_topic_results, $all_groups);

    // build rows
    $rowIndex = 0;
    $hiddenGroupIndex = 0;
    foreach($results_array as $entry){
        $rowIndex += 1;

        if(get_option('RUB_ma_hide_hidden_forums') === 'on') {
            if ($entry->visibility == 'hidden') {
                $hiddenGroupIndex += 1;
                $entry->forum_label = 'Hidden Group no. ' . $hiddenGroupIndex;
            }
        }

        $mail_body .= "<tr>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . $rowIndex . "</td>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __($entry->forum_label, 'RUB_Moderator_Analytics') . "</td>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __($entry->visibility, 'RUB_Moderator_Analytics') . "</td>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . ((property_exists($entry, "topics_written")) ? $entry->topics_written : 0) . "</td>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . ((property_exists($entry, "replies_written")) ? $entry->replies_written : 0) . "</td>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . ((property_exists($entry, "active_writers")) ? $entry->active_writers : 0) . ' / ' . ((property_exists($entry, "total_members")) ? $entry->total_members : 0) . "</td>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . ((property_exists($entry, "forums_read")) ? $entry->forums_read : 0) . "</td>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . ((property_exists($entry, "topics_read")) ? $entry->topics_read : 0) . "</td>";
        $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . ((property_exists($entry, "active_readers_forum")) ? $entry->active_readers_forum : 0) . "</td>";
        $mail_body .= '<td style="border-bottom: 1px solid">' . ((property_exists($entry, "active_readers_topics")) ? $entry->active_readers_topics : 0) . "</td>";
        $mail_body .= "</tr>";
    }

    // get totals
    $totals = rub_ma_calc_totals_for_group_results($results_array);

    // display totals in bottom row
    $mail_body .= "<tr>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . __('Total', 'RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid"></td>';
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid"></td>';
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['topics_written'] . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['replies_written'] . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . "" . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['forums_read'] . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['topics_read'] . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . "" . "</td>";
    $mail_body .= '<td style="border-top: 1px solid">' . "" . "</td>";
    $mail_body .= "</tr>";

    $mail_body .= "</table>";

    return $mail_body;
}

/**
 * Merges the different db results together
 * @param $a1
 * @param $a2
 * @param $a3
 * @param $a4
 * @return array
 */
function rub_ma_merge_group_arrays($a1, $a2, $a3, $a4){

    // assemble list of all forum ids present
    $list_forum_ids = array();
    $method_params = func_get_args();

    foreach($method_params as $param){
        foreach($param as $element){
            $list_forum_ids[] = $element->forum_id;
        }
    }

    $list_forum_ids = array_unique($list_forum_ids);

    // assemble final array through looping through arrays and merging correctly
    $result = array();
    foreach($list_forum_ids as $forum_id){

        // get objects from array
        $pos_a1 = rub_ma_objArraySearch($a1, 'forum_id', $forum_id);
        $pos_a2 = rub_ma_objArraySearch($a2, 'forum_id', $forum_id);
        $pos_a3 = rub_ma_objArraySearch($a3, 'forum_id', $forum_id);
        $pos_a4 = rub_ma_objArraySearch($a4, 'forum_id', $forum_id);

        $result[] = (object) array_merge((array) $pos_a1, (array) $pos_a2, (array) $pos_a3, (array) $pos_a4);

    }

    return $result;
}

/**
 * Gets object from array of objects.
 * http://stackoverflow.com/a/28970200
 * @param $array
 * @param $index
 * @param $value
 * @return null
 */
function rub_ma_objArraySearch($array, $index, $value) {
    foreach($array as $arrayInf) {
        if($arrayInf->{$index} == $value) {
            return $arrayInf;
        }
    }
    return null;
}

/**
 * Returns the sums of the values in the results_array. Specific to the query in rub_ma_get_stats_from_db
 * @param $results_array
 * @return array
 */
function rub_ma_calc_totals_for_group_results($results_array){

    $totals = array(
        "topics_written" => 0,
        "replies_written" => 0,
        "active_writers" => 0,
        "forums_read" => 0,
        "topics_read" => 0,
        "active_readers_forum" => 0,
        "active_readers_topics" => 0,
    );

    foreach($results_array as $entry){
        $totals['topics_written'] += (property_exists($entry, "topics_written")) ? $entry->topics_written : 0;
        $totals['replies_written'] += (property_exists($entry, "replies_written")) ? $entry->replies_written : 0;
        $totals['active_writers'] += (property_exists($entry, "active_writers")) ? $entry->active_writers : 0;
        $totals['forums_read'] += (property_exists($entry, "forums_read")) ? $entry->forums_read : 0;
        $totals['topics_read'] += (property_exists($entry, "topics_read")) ? $entry->topics_read : 0;
        $totals['active_readers_forum'] += (property_exists($entry, "active_readers_forum")) ? $entry->active_readers_forum : 0;
        $totals['active_readers_topics'] += (property_exists($entry, "active_readers_topics")) ? $entry->active_readers_topics : 0;
    }

    return $totals;
}
