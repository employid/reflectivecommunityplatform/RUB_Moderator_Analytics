<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 09-Mar-16
 * Time: 13:49
 */

/**
 * Fetches analytics data from logging table
 * @param int $weekoffset
 * @return mixed
 */
function rub_ma_get_user_stats_from_db($weekoffset = 0){
    global $wpdb;
    $table_aryo_log = $wpdb->prefix . "aryo_activity_log";

    $results = $wpdb->get_results(
        "SELECT
           IF(user_id IN (" . get_option('RUB_ma_user_moderator_id') . "), 'moderator', 'user') as user_type,
           user_id,
           SUM(IF(action = 'logged_in', 1, 0)) AS 'logins',
           SUM(IF(action = 'read' AND object_subtype = 'page' AND object_ID IN (SELECT ID FROM $wpdb->posts WHERE post_type = 'forum'), 1, 0)) AS 'forums_read',
           SUM(IF(action = 'read' AND object_subtype = 'Topics', 1, 0)) AS 'topics_read',
           SUM(IF(action = 'added' AND object_type = 'BBPress' AND object_subtype = 'topic', 1, 0)) as 'topics_written',
           SUM(IF(action = 'added' AND object_type = 'BBPress' AND object_subtype = 'reply', 1, 0)) as 'replies_written',
           SUM(IF(action = 'read' AND object_subtype = 'post', 1, 0)) AS 'news_read',
           SECOND(usr.user_registered) AS reg_date
       FROM
           $table_aryo_log AS log
       JOIN
           $wpdb->users AS usr ON log.user_id = usr.ID
       WHERE
           WEEKOFYEAR(FROM_UNIXTIME(hist_time)) = WEEKOFYEAR(NOW()) - $weekoffset
           AND YEAR(FROM_UNIXTIME(hist_time)) = YEAR(NOW())
           AND user_id NOT IN (" . get_option('RUB_ma_user_blacklist_id') . ")
       GROUP BY user_id"
    );

    return $results;
}

function rub_ma_get_number_of_registrations($weekoffset = 0){

    global $wpdb;

    $result = $wpdb->get_results("
          SELECT COUNT(*) as number FROM $wpdb->users
          WHERE
            WEEKOFYEAR(user_registered) = WEEKOFYEAR(NOW()) - $weekoffset
            AND YEAR(user_registered) = YEAR(NOW())"
    );

    return $result[0]->number;
}

/**
 * Gets the current number of registered users
 * @return mixed
 */
function rub_ma_get_total_number_of_users(){

    global $wpdb;

    $result = $wpdb->get_var(
        "SELECT COUNT(*) FROM $wpdb->users"
    );

    return $result;
}

/**
 * Creates an html table for the periodic emails based on the query in rub_ma_get_stats_from_db
 * @param $results_array
 * @return string
 */
function rub_ma_create_user_stats_html_table_for_email($results_array){

    // Set up mail content, incl. table
    $mail_body  = '<table style="border: 1px solid">';
    $mail_body .= '<tr>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('No.','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('user type','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#logins','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#topics written','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#replies written','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#forums read','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-bottom: 1px solid">' . __('#topics read','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-bottom: 1px solid">' . __('#news read','RUB_Moderator_Analytics') . '</td>';
    $mail_body .= "</tr>";

    // build rows
    $i = 0;
    foreach($results_array as $entry){
        $i += 1;

        // create fake user id
        $entry->user_type .= " " . ($entry->reg_date . $entry->user_id + 17);

        $mail_body .= "<tr>";
        $mail_body .= '<td style="border-right: 1px solid">' . $i . "</td>";
        $mail_body .= '<td style="border-right: 1px solid">' . $entry->user_type . "</td>";
        $mail_body .= '<td style="border-right: 1px solid">' . $entry->logins . "</td>";
        $mail_body .= '<td style="border-right: 1px solid">' . $entry->topics_written . "</td>";
        $mail_body .= '<td style="border-right: 1px solid">' . $entry->replies_written . "</td>";
        $mail_body .= '<td style="border-right: 1px solid">' . $entry->forums_read . "</td>";
        $mail_body .= '<td style="border-right: 1px solid">' . $entry->topics_read . "</td>";
        $mail_body .= '<td>' . $entry->news_read . "</td>";
        $mail_body .= "</tr>";
    }

    // get totals
    $totals = rub_ma_calc_totals_for_user_results($results_array);
    $numberOfUsers = rub_ma_get_total_number_of_users();
    $mods = get_option('RUB_ma_user_moderator_id');
    $mods_array = explode(",",$mods);
    $numberOfMods = count($mods_array);

    // display totals in bottom row
    $mail_body .= "<tr>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . __('Total', 'RUB_Moderator_Analytics') . '</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $i . ' / ' . $numberOfUsers . ' (incl.  ' . $numberOfMods . ' mod(s))</td>';
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['logins'] . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['topics_written'] . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['replies_written'] . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['forums_read'] . "</td>";
    $mail_body .= '<td style="border-right: 1px solid; border-top: 1px solid">' . $totals['topics_read'] . "</td>";
    $mail_body .= '<td style="border-top: 1px solid">' . $totals['news_read'] . "</td>";
    $mail_body .= "</tr>";

    $mail_body .= "</table>";

    return $mail_body;
}

/**
 * Returns the sums of the values in the results_array. Specific to the query in rub_ma_get_stats_from_db
 * @param $results_array
 * @return array
 */
function rub_ma_calc_totals_for_user_results($results_array){

    $totals = array(
        "user_type" => 0,
        "logins" => 0,
        "forums_read" => 0,
        "topics_read" => 0,
        "topics_written" => 0,
        "replies_written" => 0,
        "news_read" => 0,
    );

    foreach($results_array as $entry){
        $totals['user_type'] += $entry->user_type;
        $totals['logins'] += $entry->logins;
        $totals['forums_read'] += $entry->forums_read;
        $totals['topics_read'] += $entry->topics_read;
        $totals['topics_written'] += $entry->topics_written;
        $totals['replies_written'] += $entry->replies_written;
        $totals['news_read'] += $entry->news_read;
    }

    return $totals;
}