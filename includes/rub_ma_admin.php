<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 03-Mar-16
 * Time: 18:01
 */

/**
 * Create a new entry in the menu bar for all RUB plugins (if not already created). Then add entry for this plugin.
 */
function rub_ma_admin_menu(){

    // check whether global RUB admin menu page already exists, if not, create it!
    if ( empty ( $GLOBALS['admin_page_hooks']['RUB-reflection-plugins'] ) )
        add_menu_page(
            'RUB Reflection Plugins',
            'RUB Reflection Plugins',
            'manage_options',
            'RUB-reflection-plugins',
            'rub_ma_admin_menu_page_callback'
        );

    // afterwards, add sub entry for this plugin
    add_submenu_page(
        'RUB-reflection-plugins',
        'Moderator Analytics',
        'Moderator Analytics',
        'manage_options',
        'Moderator-Analytics',
        'rub_ma_admin_settings_form'
    );
}
add_action('admin_menu', 'rub_ma_admin_menu');

/**
 * Define a settings section which holds our settings
 */
function rub_ma_settings(){
    add_settings_section(
        'RUB-ma-settings',
        __('Moderator Analytics Settings','RUB_Moderator_Analytics'),
        'RUB_ma_settings_callback',
        'Moderator-Analytics'
    );

    // Field to save the email addresses of the main moderators
    add_settings_field(
        'RUB_ma_emails_id',
        __('Emails of moderators who should get summary','RUB_Moderator_Analytics'),
        'RUB_ma_callback',
        'Moderator-Analytics',
        'RUB-ma-settings',
        array(
            'RUB_ma_emails_id' // Should match Option ID
        )
    );

    // field to store users whose activity should be excluded
    add_settings_field(
        'RUB_ma_user_blacklist_id',
        __('List of user IDs not to include in the summary','RUB_Moderator_Analytics'),
        'RUB_ma_callback',
        'Moderator-Analytics',
        'RUB-ma-settings',
        array(
            'RUB_ma_user_blacklist_id' // Should match Option ID
        )
    );

    // field to store users should be marked as moderator activity
    add_settings_field(
        'RUB_ma_user_moderator_id',
        __('List of user IDs who should be labelled as moderator','RUB_Moderator_Analytics'),
        'RUB_ma_callback',
        'Moderator-Analytics',
        'RUB-ma-settings',
        array(
            'RUB_ma_user_moderator_id' // Should match Option ID
        )
    );

    add_settings_field(
        'RUB_ma_hide_hidden_forums',
        __('Anonymize hidden forums?','RUB_Prompting'),
        'rub_ma_checkbox_settings_callback',
        'Moderator-Analytics',
        'RUB-ma-settings',
        array(
            'RUB_ma_hide_hidden_forums' // Should match Option ID
        )
    );

    // separate settings section for the immediate sending feature
    add_settings_section(
        'RUB-ma-settings_extra_send',
        __('Send mail again','RUB_Moderator_Analytics'),
        'RUB_ma_settings_callback',
        'Moderator-Analytics-extra'
    );

    // Field to save the email addresses of the main moderators
    add_settings_field(
        'RUB_ma_emails_id_extra',
        __('Email','RUB_Moderator_Analytics'),
        'RUB_ma_callback',
        'Moderator-Analytics-extra',
        'RUB-ma-settings_extra_send',
        array(
            'RUB_ma_emails_id_extra' // Should match Option ID
        )
    );

    register_setting('Moderator-Analytics', 'RUB_ma_emails_id', 'esc_attr');
    register_setting('Moderator-Analytics', 'RUB_ma_user_blacklist_id', 'esc_attr');
    register_setting('Moderator-Analytics', 'RUB_ma_user_moderator_id', 'esc_attr');
    register_setting('Moderator-Analytics', 'RUB_ma_hide_hidden_forums', 'esc_attr');

    register_setting('Moderator-Analytics-extra', 'RUB_ma_emails_id_extra', 'rub_ma_validate_and_send');
}
add_action('admin_init', 'rub_ma_settings');

/**
 * This method handles the display of the settings section in the admin menu
 */
function rub_ma_admin_settings_form() {
    ?>
    <div class="wrap">
        <form method="post" action="options.php">
            <?php
            //wp_nonce_field('update-options');
            settings_errors();
            settings_fields( 'Moderator-Analytics' );
            do_settings_sections( 'Moderator-Analytics' );
            submit_button();
            ?>
        </form>
    </div>
    <div class="wrap">
        <form method="post" action="options.php">
            <?php
            //wp_nonce_field('update-options');
            settings_errors();
            settings_fields( 'Moderator-Analytics-extra' );
            do_settings_sections( 'Moderator-Analytics-extra' );
            submit_button("Save address & send mail");
            ?>
        </form>
    </div>
    <?php
}

/**
 * Callback method to display input form for the questions
 * @param $args
 */
function rub_ma_callback($args) {
    // dont use translation here, let qTranslate-X do the work!

    $value = get_option($args[0]);
    echo '<input
        type="text"
        id="'.$args[0].'"
        class="'.$args[0].'"
        name="'.$args[0].'"
        size="50"
        value="'.$value.'"
        >';
    echo '<p class="description">'.$args[0].'</p>';
}

/**
 * This callback just shows the description for the settings section
 */
function RUB_ma_settings_callback(){
    _e('Separate values by ","!!', 'RUB_Moderator_Analytics');
}

/**
 * This callback can be used to display information on the overall admin page (not the subentry)
 */
function rub_ma_admin_menu_page_callback(){
    // Maybe show later all currently active plugins
}

/**
 * Enables tracking of field updates
 * @param $option
 * @param $old_value
 * @param $value
 */
function rub_ma_settings_updated($option, $old_value, $value){

    if($option == 'RUB_ma_emails_id'){
        do_action('RUB_ma_emails_id', $option, $old_value, $value);
    }

    if($option == 'RUB_ma_user_blacklist_id'){
        do_action('RUB_ma_user_blacklist_id', $option, $old_value, $value);
    }
}
add_action('updated_option', 'rub_ma_settings_updated', 10, 3);

/**
 * Special validation function, to also send the mail upon saving the value.
 * @param $input
 * @return mixed
 */
function rub_ma_validate_and_send($input){
    $validated = sanitize_email($input);
    rub_ma_send_summary(true);

    return $validated;
}

/**
 * Callback method to display checkbox input form
 * @param $args
 */
function rub_ma_checkbox_settings_callback($args) {
    // dont use translation here, let qTranslate-X do the work!

    $value = get_option($args[0]);
    if($value === "on"){
        $value = 'checked="checked"';
    }

    echo '<input
        type="checkbox"
        id="'.$args[0].'"
        class="'.$args[0].'"
        name="'.$args[0].'"
        size="50"
        ' . $value . '>';
    echo '<p class="description RUB_p_setting_checkbox">'.$args[0].'</p>';
}