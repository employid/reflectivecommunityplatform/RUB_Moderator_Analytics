<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 03-Mar-16
 * Time: 17:06
 */

/*
Plugin Name: RUB Moderator Analytics
Plugin URI: http://not-available.yet
Description: This plugin sends a weekly summary of the activities on the platform per user (number of topic reads, posts, comments, news reads, logins)
Version: 0.1
Author: Oliver Blunk
Author URI: http://www.imtm-iaw.ruhr-uni-bochum.de/wissenschaftliches-personal/oliver-blunk/
License: All rights reserved
*/

// TODO: remove people with only 0 value entries from the table! (e.g. we someone got joined into a group... rare case)

include('includes/rub_ma_cron.php');
include('includes/rub_ma_admin.php');
include('includes/rub_ma_stats_group.php');
include('includes/rub_ma_stats_user.php');

function rub_ma_activate_plugin(){
    rub_ma_set_schedule();
}
register_activation_hook(__FILE__, 'rub_ma_activate_plugin');

function rub_ma_deactivate_plugin(){
    wp_clear_scheduled_hook('rub_ma_weekly_summary');
}
register_deactivation_hook(__FILE__, 'rub_ma_deactivate_plugin');

/*
 * load language files
 */
function rub_ma_load_textdomain() {
    load_plugin_textdomain( 'RUB_Moderator_Analytics', false, basename(dirname(__FILE__)) . '/languages' );
}
add_action('plugins_loaded', 'rub_ma_load_textdomain' );

/*
function rub_ma_foo(){
     // rub_ma_send_summary();
}
add_action('init','rub_ma_foo');
*/